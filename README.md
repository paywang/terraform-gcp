
# VPC and GKE Demo
This is to demo terraform capability to do the following:

1. Create two GCP VPC networks with public and private subnets.  
2. Create vpc peering between the two VPC networks
3. Create a bastion server in public subnet
4. Create a server in private subnet which can be accessed by bastion server
5. Create GKE cluster in private subnet
6. Deploy an application on the GKE cluster 

![alt text](https://gitlab.com/paywang/terraform-gcp/-/raw/master/img/image01.png)


# Usage: 

1. Install latest of Terraform v0.12.x and gcloud (I tried with terraform 0.12.29 and gcloud 315.0.0)
1. Export GOOGLE_APPLICATION_CREDENTIALS to your gcp key file.   
i.e. `export GOOGLE_APPLICATION_CREDENTIALS=~/Documents/gcp-key/etg-prj-hyc-ibm-power-beta-1629154d27a2.json`
1. Create a bucket for terraform state file.  Enter the bucket name is in terraform backend in network/main.tf .
i.e. `bucket  = "tf-state-demo1-essextec"`


```
cd network
1. Run `terraform init`
2. Run `terraform plan`
3. If the plan looks good, run `terraform apply`

```

```
cd gke
1. Run `terraform init`
2. Run `terraform plan`
3. If the plan looks good, run `terraform apply`

```

```
cd app
1. Connect to GKE cluster:
gcloud container clusters get-credentials demo-power-cluster2 --region us-east4 \
--project etg-prj-hyc-ibm-power-beta
2. kubectl apply -f ngnix-deployment.yaml
```

# CI/CD:
1. This repo has AutoDevOps pipeline defined in .gitlab-ci.yml
1. The pipeline will deploy terraform change into GCP project    
 
