terraform {
  # The modules used in this example have been updated with 0.12 syntax, which means the example is no longer
  # compatible with any versions below 0.12.
  required_version = ">= 0.12"
  backend "gcs" {
    bucket  = "tf-state-demo1-essextec"
    prefix  = "terraform/state/eks"
  }
}

# ---------------------------------------------------------------------------------------------------------------------
# Create a GKE cluster
# ---------------------------------------------------------------------------------------------------------------------

module "kubernetes-engine" {
  //source      = "terraform-google-modules/kubernetes-engine/google"
  source    = "../modules/kubernetes-engine"
  # insert the 9 required variables here
  project_id                 = var.project
  name                       = join ("-", compact([var.name_prefix, "cluster2"]))
  region                     = var.region
  zones                      = [var.zone]
  network                    = "demo-power-2-network"
  subnetwork                 = "demo-power-2-subnetwork-private"
  //ip_range_pods              = "private-services"
  //ip_range_services          = "private-services2"
  ip_range_pods              = "10.2.0.0/20"
  ip_range_services          = "10.3.0.0/24"
  http_load_balancing        = false
  horizontal_pod_autoscaling = true
  network_policy             = true

  node_pools = [
    {
      name               = "default-node-pool"
      machine_type       = "e2-medium"
      node_locations     = "us-east4-a"
      min_count          = 1
      max_count          = 3
      local_ssd_count    = 0
      disk_size_gb       = 30
      disk_type          = "pd-standard"
      image_type         = "COS"
      auto_repair        = true
      auto_upgrade       = true
      //service_account    = "project-service-account@etg-prj-hyc-ibm-power-beta.iam.gserviceaccount.com"
      preemptible        = false
      initial_node_count = 1
    },
  ]

  node_pools_oauth_scopes = {
    all = []

    default-node-pool = [
      "https://www.googleapis.com/auth/cloud-platform",
    ]
  }

  node_pools_labels = {
    all = {}

    default-node-pool = {
      default-node-pool = true
    }
  }

  node_pools_metadata = {
    all = {}

    default-node-pool = {
      node-pool-metadata-custom-value = "my-node-pool"
    }
  }

  node_pools_taints = {
    all = []

    default-node-pool = [
      {
        key    = "default-node-pool"
        value  = true
        effect = "PREFER_NO_SCHEDULE"
      },
    ]
  }

  node_pools_tags = {
    all = []

    default-node-pool = [
      "default-node-pool",
    ]
  }
}
